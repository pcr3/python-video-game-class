"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import arcade
import os

from settings_global import PLAYER_SCALING

def create_animated_player(texture_path: str) -> arcade.AnimatedWalkingSprite:
    """Create a animated walking player or a flip picture book animation
    Think of a turn base video game: After each action, you freeze
    Thus, a PNG image for each action (i.e., walking) is
    required to make the player sprite animated

    For Example: You create four PNG images
                1) Standing stilll
                2) Pickup foot with knee bent
                3) Extend foot with Knee bent
                4) Put foot down
        The order for walking would be: 1->2->3->4->1->2->3->4->1...
    """

    # !!! IMPORANT !!!
    # READ THIS
    # NOTE: YOU MUST COMPLETE ALL OF THE TASKS FOR IT TO WORK
    """ List of images for the example sprite

        - alienBlue_front.png
        - alienBlue_jump.png
        - alienBlue_walk1.png
        - alienBlue walk2.png

        There are 6 variables to use to solve Task 1-4
            1) alien_front
            2) alien_walk_right_1
            3) alien_walk_right_2
            4) alien_walk_left_1
            5) alien_walk_left_2
            6) alien_jump
    """
    # BONUS:
    '''Find your own png images to use!
    '''

    # DO NOT CHANGE THESE VALUES TILL YOU DO ALL TASKS
    # NOTE: Could we use more? What does each one mean?
    thing_front = arcade.load_texture(os.path.join(
        texture_path,
        "alienBlue_front.png"))

    thingwalkright1 = arcade.load_texture(os.path.join(
        texture_path,
        "alienBlue_walk1.png"))

    thingwalkright2 = arcade.load_texture(os.path.join(
        texture_path,
        "alienBlue_walk1.png"))

    thingwalkleft1 = arcade.load_texture(os.path.join(
        texture_path,
        "alienBlue_walk1.png"), mirrored=True)

    #NOTE: What does mirroed do?
    thingwalkleft2 = arcade.load_texture(os.path.join(
        texture_path,
        "alienBlue_walk1.png"), mirrored=True)

    # NOTE: MUST READ! HINT BELOW!
    """
        # NOTE: Order is important. How do you walk in real life?
        # NOTE: What is the difference between going left or right?

        Example Python List:
            - A list in Python has elements:
                MyList = [element1, element2, ...]
            - You can access/get each element by going in order starting with 0
            - 0 is the same as being first in programming

            List[0] == element1
            List[1] == element2


            # NOTE IMPORTANT!!!
            # Tasks 1-6 must be completed for a change to show up!
            # If a error comes up, then something is wrong!
    """

    # TODO: TASK 1
    """
        # Collect WALKING right animations
        # This list expects two elements
    """
    walking_right_animations = [] # Expects at least two elements

    # TODO: TASK 2
    """
        # Collect WALKING left animations
        # This list expects two element
    """
    walking_left_animations = []  # Expects at least two elements

    # TODO: TASK 3
    """
        # Standing animations
        # This list expects one animation
    """

    standing_animation = []  # Expects at least one elements

    # TODO: TASK 4
    # Is the player scaling correct (i.e., sprite size)?
    player = arcade.AnimatedWalkingSprite(PLAYER_SCALING)

    # TODO: TASK 5
    """
        # Assign each animation list to the correct variable
        # Each <replace_me> is expecting a animation list
        # Remove the hashtag ('#') to uncomment

        NOTE: The game needs to see a list of static images or textures
        To make something animated, the game will rotate through a list
        of static images. Compare this to a flip picture book that makes
        the drawings become animated
    """
    player.stand_left_textures  = standing_animation
    player.stand_right_textures = standing_animation
    player.walk_left_textures   = walking_left_animations
    player.walk_right_textures  = walking_right_animations

    # TODO: TASK 6
    """
        # Where should the player start in the map?
        # Each <replace_me> is expecting a positive integer
    """
    player.center_x = 0
    player.center_y = 0

    # TODO: TASK 7
    """
        # What PNG should be loaded at the start of the game?
        # <replace_me> expects a single animation element type not a list
    """
    #player.texture = thing_front

    if not player.texture or not player.walk_right_textures:
        print("MISSING: Player textures!!!")
        return None

    # DO NOT CHANGE
    return player
