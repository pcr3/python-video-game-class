"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from typing import Tuple

def player_respawn_conditions(
    player_hp        : int,
    player_x_position: int,
    player_y_position: int,
    time_remaining   : int) -> bool:
    """This determines if the player should respawn
    What conditions cause GAME OVER to appear?

        Params:
            player_hp         (int): Current hp of the player: [0, MAX_HEALTH]
            player_x_position (int): Current horizontal position of the player (x-axis)
            player_y_position (int): CUrrent vertical position of the player (y-axis)
            time_reamining    (int): Current time left in seconds: [0, TIME_LIMIT]

        Returns:
            game_over (bool): True if game is over, false otherwise

        For example: If the player falls off the map, should the game end?
    """
    # DO NOT CHANGE
    game_over = False

    # EXAMPLE: Player falls off the map, so the game is over
    if player_y_position < 0:
        # If the vertical position is negative, the player fell off the screen!
        game_over = True

    # TODO: TASK 1
    """Determine when game over is achieved based on the players health

        Step 1): Create a IF condition that matches your rule
        Step 2): Set the variable game_over to False
    """

    # TODO: TASK 2
    """Determine when game over is achieved based on the time remaining

        Step 1): Create a IF condition that matches your rule
        Step 2): Set the variable game_over to False
    """

    # TODO: TASK 3
    """Determine when game over is achieved based on the x position
    Can the player run off the map at the end of the game?

        Step 1): Create a IF condition that matches your rule
        Step 2): Set the variable game_over to False
    """

    # DO NOT CHANGE
    return game_over

def player_respawn_location(
    check_point_flag: bool) -> Tuple[int, int]:
    """Return the position the player should respawn at
    In cartesian cordinates (x,y)

        Returns:
            spawn_location: x = horizontal location (x-axis)
                            y = vertical location   (y-axis)
    """

    # TODO: TASK 4
    """Where should the player respawns?
    Expected value is a tuple or pair = (Integer, Integer)

    """
    # NOTE: Is (950, 30) a good spot to respawn at?
    spawn_location = (950, 30) # Change these values

    # BONUS: Check to see if check_point_flag is true
    # If so, assign a new respawn location!

    return spawn_location
