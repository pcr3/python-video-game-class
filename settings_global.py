"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

##########################################################
# NOTE: This file contains constants used by the game    #
#        - Player physics/Settings                       #
#        - World Physics/Setting                         #
#        - Mob Physics/Settings                          #
#        - Projctile Physics/Settings                    #
##########################################################

# Play around with these values to see what happens!

# Game Window Size
GAME_HEIGHT = 800
GAME_WIDTH  = 1000

# PLAYER CONSTANTS
JUMP_SPEED         = 16
MOVEMENT_SPEED     = 5
MAX_HEALTH         = 100
DAMAGE             = 5
POWERUP_MULTIPLYER = 1.2
PROJECTILE_COUNT   = 40
PLAYER_SCALING     = 0.5

# PROJECTILE CONSTANTS
PROJECTILE_SCALING = 0.5
PROJECTILE_SPEED   = 5

# MOB CONSTANTS
MOB_DAMAGE     = 100
MOB_SPEED      = 1
MOB_SCALING    = 1
MOB_HP         = 10
MOB_JUMP_SPEED = 5

# MAP/WORLD CONSTANTS
TILE_SCALING      = 0.5
SPRITE_PIXEL_SIZE = 128
GRID_PIXEL_SIZE   = SPRITE_PIXEL_SIZE * TILE_SCALING

# MESS WITH GRAVITY! What happens?
GRAVITY = 1.2

# TODO: BOSS
"""The boss moves to fast and is to hard to fight!
    Step 1): What should the BOSS_HP be?
    Step 2): What should the BOSS_DMG be?
    Step 3): What should the BOSS_SPEED be?
    Step 4): What should the BOSS_JUMP_SPEED be?
"""
BOSS_SCALING    = 1.5
BOSS_HP         = 5000
BOSS_DMG        = 100
BOSS_SPEED      = 10
BOSS_JUMP_SPEED = 2
BOSS_X_SPAWN    = 4500
BOSS_Y_SPAWN    = 120
BOSS_PATHS = [[4700, BOSS_Y_SPAWN],[4400, BOSS_Y_SPAWN]]

